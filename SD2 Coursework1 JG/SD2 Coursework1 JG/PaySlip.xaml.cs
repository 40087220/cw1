﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SD2_Coursework1_JG
{
    /// <summary>
    /// Interaction logic for PaySlip.xaml
    /// Author Jordan Gray
    /// Matriculation Number 40087220
    /// Date Last Modified 24/10/2014
    /// 
    /// This Class creates a payslip object.
    /// This payslip object displays values entered from the staff class/Main window.
    /// 
    /// This creates a new window.
    /// </summary>
    public partial class PaySlip : Window
    {
        public PaySlip()
        {
            InitializeComponent();
        }

        //Close button.
        //Closes the window when clicked.
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
           
            this.Close();
        }

        //Display the values/calculated values that have been generated from the previous window.
        public void display(string Firstname, string SecondName, string Department, int Hours, int PayRate, double NetPay, double GrossPay, double TaxPerc)
        {
            FirstNameLbl.Content = Firstname;
            SurNameLbl.Content = SecondName;
            DepartmentLbl.Content = Department;
            HoursWorkedLbl.Content = Hours;
            GrossPayLbl.Content = GrossPay;
            TaxRateLbl.Content = TaxPerc;
            NetPayLbl.Content = NetPay;

        }

        
    }
}
