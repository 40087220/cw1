﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SD2_Coursework1_JG
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Author Jordan Gray
    /// Matriculation Number 40087220
    /// Date Last Modified 24/10/2014
    /// </summary>
    public partial class MainWindow : Window
    {
        Staff s1 = new Staff();

        public MainWindow()
        {
            InitializeComponent();
        }

        //Set button.
        //This will set the values that the user inputs in the main window.
        private void SetButton_Click(object sender, RoutedEventArgs e)
        {
            s1.FirstName = FirstNameBox.Text;
            s1.SecondName = SecondNameBox.Text;
            s1.DOB = DOBBox.Text;
            s1.Department = DepartmentBox.Text;
            s1.Hours = int.Parse(HoursWorkedBox.Text);

            //This IF statement is a range check.
            //It checks to make sure thaat the Staff ID the user has entered is within the correct range (1000 - 2000).
            if(s1.StaffID >1000 || s1.StaffID < 2000)
            {
                s1.StaffID = int.Parse(StaffIDBox.Text);
            }
            else
            {
                MessageBox.Show("Staff ID must be within the range of 1000 - 2000. Please Re-enter");
            }

            //This IF statement is a range check.
            //This range checks the user input for the pay rate and displays a message box if incorrect 
            //Between £0 - £999
            if(s1.PayRate <0 || s1.PayRate > 999)
            {
                MessageBox.Show("Pay Rate must be within the range of 0 - 999. Please Re-enter");
            }
            else
            {
                s1.PayRate = int.Parse(PayRateBox.Text);
            }

            //This IF statement is a range check.
            //This range checks the user input for hours worked and displays a message box if incorrect.
            //Hours 1 - 65.
            if(s1.Hours < 1 || s1.Hours > 65)
            {
                MessageBox.Show("Hours Worked must be within the range of 1 - 65. Please Re-enter");
            }
            else
            {
                s1.Hours = int.Parse(HoursWorkedBox.Text);
            }

        }

        //Clear button.
        //Removes contents from the text boxes.
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            FirstNameBox.Clear();
            SecondNameBox.Clear();
            DOBBox.Clear();
            DepartmentBox.Clear();
            HoursWorkedBox.Clear();
            StaffIDBox.Clear();
            PayRateBox.Clear();
        }

        //Get Button.
        //Uses the values from the staff class to update the text boxes.
        private void GetButton_Click(object sender, RoutedEventArgs e)
        {
            FirstNameBox.Text = s1.FirstName;
            SecondNameBox.Text = s1.SecondName;
            DOBBox.Text = s1.DOB;
            DepartmentBox.Text = s1.Department;
            HoursWorkedBox.Text = s1.Hours.ToString();
            StaffIDBox.Text = s1.StaffID.ToString();
            PayRateBox.Text = s1.PayRate.ToString();
            
            
        }



        //Calculate Pay button.
        //A simple dialogue will be displayed asking for the number of hours worked. Range = 1 - 48 hours.
        //This creates a second window to be displayed in the form of a payslip.
        private void CalcPayButton_Click(object sender, RoutedEventArgs e)
        {
            
            //Clicking the Calculate button shows the payslip window.
            PaySlip payslip = new PaySlip();
            payslip.Show();

            s1.calcPay(s1.Hours);
            s1.calcTaxRate(s1.GrossPay);
            s1.calcNetPay(s1.GrossPay, s1.taxdeduct);

            //Set up the values to be displayed in the payslip window.
            payslip.display(s1.FirstName, s1.SecondName, s1.Department, s1.Hours, s1.PayRate, s1.NetPay, s1.GrossPay, s1.TaxPerc);

            
        }
        
    }
}
