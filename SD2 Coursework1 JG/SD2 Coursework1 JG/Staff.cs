﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Author Jordan Gray
 * 40087220
 * Coursework 1 for software development 2
 * Date last modified 24/10/14
 * */

namespace SD2_Coursework1_JG
{
    //This initialises the staff objects variables and allows for creation of staff objects.
    class Staff
    {
        //A basic constructor for the staff class.
        public Staff()
        {

        }

        //Initialise variables in the staff class.
        private int staffid;    //Staff ID
        private string firstname;   //First Name
        private string secondname;  //Second Name
        private string dob; //Date of birth
        private string department;  //Department
        private int payrate;    //Pay Rate
        private int hours;  //Hours Worked
        public double taxdeduct;    //Tax Deduction
        private double grosspay;    //Gross Pay
        private double netpay;  //Net Pay
        private double taxperc; //Tax Percentage

        //Initialise setters and getters for the staff class.
        public int StaffID
        {
            get { return staffid; }
            set { staffid = value; }
        }
        public string FirstName
        {
            get { return firstname; }
            set { firstname = value; }

        }
        public string SecondName
        {
            get { return secondname; }
            set { secondname = value; }
        }
        public string Department
        {
            get { return department; }
            set { department = value; }
        }
        public string DOB
        {
            get { return dob; }
            set { dob = value; }
        }
        public int PayRate
        {
            get { return payrate; }
            set { payrate = value; }
        }
        public int Hours
        {
            get { return hours; }
            set { hours = value; }
        }
        public double GrossPay
        {
            get { return grosspay; }
            set { grosspay = value; }
        }
        public double NetPay
        {
            get { return netpay; }
            set { netpay = value; }
        }
        public double TaxPerc
        {
            get { return taxperc; }
            set { taxperc = value; }
        }

        //A method to calculate the payment due to a member of staff.
        //Payment will be returned in pounds and pence.
        public void calcPay(int hours)
        {
            //Calculation to work out the gross pay for a staff member.
            grosspay = payrate * hours;

        }
        //End calcPay.

        //A method that calculates the tax to be deducted from staffs pay.
        //If the gross pay of a staff member is less than £1000 then tax is 10%.
        //If gross pay of a staff member is more than 1000 then tax is 20%.
        public void calcTaxRate(double grosspay)
        {
            if (grosspay < 1000)
            {
                taxperc = 0.1;

            }
            else
            {
                taxperc = 0.2;
            }

            taxdeduct = grosspay * taxperc;


        }
        //End calcTaxRate.

        //A method that calculates the net pay of the user/staff member.
        //Takes in the gross pay and deducts the current tax percentage.
        public void calcNetPay(double grosspay, double taxdeduct)
        {
            netpay = grosspay - taxdeduct;
        }
        //End calcNetPay.



    }
}

