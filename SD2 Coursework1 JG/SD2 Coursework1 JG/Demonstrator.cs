﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD2_Coursework1_JG
{
    // Author Jordan Gray
    // Matriculation Number 40087220
    // Date Last Modified 24/10/2014
    // This class creates a seperate method for the demonstrator object for calculating pay.
    // This class inherits from the Staff class.
    class Demonstrator : Staff
    {
        //Basic constructor for demonstrator class.
        public Demonstrator()
        {

        }

        //Initialising variable used by this class.
        private string course;
        private string supervisor;

        //Creating setters and getters for variables.
        public string Course
        {
            get { return course; }
            set { course = value; }

        }

        //A seperate method/function to staff to calculate tax rate and pay.
        //Calculates tax at 5% (Rather than the staff class' 10% or 20%).
        //This is for the demonstrator only.
        public void calculateTaxRate(double GrossPay)
        {
            TaxPerc = 0.05;

            taxdeduct = GrossPay * TaxPerc;
            NetPay = GrossPay - taxdeduct;
        }
    }
}
